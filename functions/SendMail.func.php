<?php
/**
 * 发送邮件的函数库
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/1/21
 * Time: 23:41
 */

/**
 * 发送邮件
 * @param string $host      smtp地址
 * @param string $fromEmail 发件邮箱
 * @param string $fromPwd   发件密码
 * @param string $fromName  发件人
 * @param string $toEmail   收件邮箱
 * @param string $toName    收件人
 * @param string $subject   主题
 * @param string $content   正文
 * @return bool|string
 * @throws phpmailerException
 */
function sendMail($host, $fromEmail, $fromPwd, $fromName, $toEmail, $toName, $subject, $content)
{
    $mail = new PHPMailer;

    //使用smtp服务器
    $mail->isSMTP();

    //smtp地址
    $mail->Host = $host;

    //是否需要验证
    $mail->SMTPAuth = true;

    //邮件内容编码（注意：默认是iso的，需要变更为utf-8）
    $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';

    //邮箱账号以及密码
    $mail->Username = $fromEmail;
    $mail->Password = $fromPwd;

    //设置发件邮箱以及收件人
    $mail->From = $fromEmail;
    $mail->FromName = $fromName;

    //设置收件邮箱以及收件人
    $mail->addAddress($toEmail, $toName);

    //设置邮件内容为html格式
    $mail->isHTML(true);

    //设置主题
    $mail->Subject = $subject;

    //设置正文
    $mail->msgHTML($content);

    $res = $mail->send();

    if ($res !== false) {
        return true;
    } else {
        return 'Mailer Error: ' . $mail->ErrorInfo;
    }
}