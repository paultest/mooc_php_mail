<?php
/**
 * mysql简单实现队列发送邮箱
 *
可以优化的空间还很多，比如说把数据库连接弄成一个类，数据表可以加内容字段、主题字段以及发送人等来存放需要发送的内容、主题和发送人等，队列需要加锁防止多个进程同时对某一个记录操作，记录错误，以及出错的回滚操作等
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/1/21
 * Time: 23:56
 */

//绝对路径引入类
$root_path = dirname(__FILE__);
require $root_path . '/PHPMailer/PHPMailerAutoload.php';

//绝对路径引入发送邮件函数
require $root_path . '/functions/SendMail.func.php';

//连接数据库
$link = new PDO('mysql:host=localhost;dbname=test', 'root', '123456');

$sql = 'SELECT * FROM task_list WHERE status = 0 ORDER BY id ASC LIMIT 5';
$res = $link->query($sql);
$result = [];
while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
    $result[] = $row;
}

$host = 'smtp.aliyun.com';
$fromEmail = 'jiandanok@aliyun.com';
$fromPwd = '844865712Aa';
$fromName = 'aliyun';
$toName = '简单ok';
$subject = 'mysql+测试队列+优化版';

if (empty($result) === false) {
    foreach ($result as $k=>$v) {
        $content = '<h1>第' . ($k+1) . '封邮件</h1>';
        $send = sendMail($host, $fromEmail, $fromPwd, $fromName, $v['user_email'], $toName, $subject, $content);
        if ($send === true) {
            //需要把队列表中已发送的队列设置为已发送
            $link->query("UPDATE task_list SET status = 1 WHERE id = ".$v['id']);
        }

        //每隔3秒发一次邮件
        sleep(3);
    }
}
echo "done";
